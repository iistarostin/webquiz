import React from 'react';
import { shallow } from 'enzyme';
import { TestPage } from '../../../src/features/home/TestPage';

describe('home/TestPage', () => {
  it('renders node with correct class name', () => {
    const props = {
      home: {},
      actions: {},
    };
    const renderedComponent = shallow(<TestPage {...props} />);

    expect(renderedComponent.find('.home-test-page').length).toBe(1);
  });
});
