import {
  HOME_INPUT_CHANGED,
} from '../../../../src/features/home/redux/constants';

import {
  inputChanged,
  reducer,
} from '../../../../src/features/home/redux/inputChanged';

describe('home/redux/inputChanged', () => {
  it('returns correct action by inputChanged', () => {
    expect(inputChanged()).toHaveProperty('type', HOME_INPUT_CHANGED);
  });

  it('handles action type HOME_INPUT_CHANGED correctly', () => {
    const prevState = {};
    const state = reducer(
      prevState,
      { type: HOME_INPUT_CHANGED }
    );
    // Should be immutable
    expect(state).not.toBe(prevState);

    // TODO: use real case expected value instead of {}.
    const expectedState = {};
    expect(state).toEqual(expectedState);
  });
});
