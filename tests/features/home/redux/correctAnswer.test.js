import {
  HOME_CORRECT_ANSWER,
} from '../../../../src/features/home/redux/constants';

import {
  correctAnswer,
  reducer,
} from '../../../../src/features/home/redux/correctAnswer';

describe('home/redux/correctAnswer', () => {
  it('returns correct action by correctAnswer', () => {
    expect(correctAnswer()).toHaveProperty('type', HOME_CORRECT_ANSWER);
  });

  it('handles action type HOME_CORRECT_ANSWER correctly', () => {
    const prevState = {};
    const state = reducer(
      prevState,
      { type: HOME_CORRECT_ANSWER }
    );
    // Should be immutable
    expect(state).not.toBe(prevState);

    // TODO: use real case expected value instead of {}.
    const expectedState = {};
    expect(state).toEqual(expectedState);
  });
});
