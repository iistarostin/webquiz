// Rekit uses a new approach to organizing actions and reducers. That is
// putting related actions and reducers in one file. See more at:
// https://medium.com/@nate_wang/a-new-approach-for-managing-redux-actions-91c26ce8b5da

import {
  HOME_NEXT_PAGE,
} from './constants';

import initialState from './initialState';

import React from 'react'

export function nextPage() {
  return {
    type: HOME_NEXT_PAGE,
  };
}

function multipleChoiceAnswer(text, isCorrect) {
  this.text = text;
  this.isCorrect = isCorrect;
}

function multipleChoiceQuestion (title, text, answers, value) {
    this.title = title;
    this.questionType = "mc";
    this.text = text;
    this.answers = answers;
    this.value = value;
}

function shortAnswerQuestion (title, text, correct, value) {
    this.title = title;
    this.questionType = "sa";
    this.text = text;
    this.correct = correct;
    this.value = value;
}

function getNextQuestion(index) {
  const questions = [
    new multipleChoiceQuestion("Question 1", "2 + 2 equals", 
      [
        new multipleChoiceAnswer("3", false),
        new multipleChoiceAnswer("4", true), 
        new multipleChoiceAnswer("5", false)
      ], 1),
    new multipleChoiceQuestion("Question 2", "Proposition (a->b)->((b->c)->(a->c)) is", 
    [
      new multipleChoiceAnswer("Contradictory", false),
      new multipleChoiceAnswer("Satisfiable and Falsifiable", false), 
      new multipleChoiceAnswer("Tautologic", true)
    ], 5),
    new shortAnswerQuestion("Question 3", "How many derangements are possible on 10 objects?", "1334961", 7),
    new shortAnswerQuestion("Question 4", "What is the number of different boolean functions of 4 variables?", 65536, 10)
  ];
  
  return (index >= 0 && index < questions.length) ? questions[index] : null;
}

export function reducer(state, action) {
  switch (action.type) {
    case HOME_NEXT_PAGE:
      let newIndex = state.stateIndex + 1;
      if (newIndex == -1) {
        return initialState;
      }
      let newQuestion = getNextQuestion(newIndex);
      let newScore = state.score;
      if (newIndex != -1 && newQuestion == null) {
        newIndex = -2;
      }
      if (state.question != null) {
        newScore += state.questionScore * state.question.value;
      }
      return {
        ...state,
        stateIndex: newIndex,
        question: newQuestion,
        score: newScore,
        questionScore: 0
      };

    default:
      return state;
  }
}
