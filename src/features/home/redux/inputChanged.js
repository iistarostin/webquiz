// Rekit uses a new approach to organizing actions and reducers. That is
// putting related actions and reducers in one file. See more at:
// https://medium.com/@nate_wang/a-new-approach-for-managing-redux-actions-91c26ce8b5da

import {
  HOME_INPUT_CHANGED,
} from './constants';

export function inputChanged(value) {
  return {
    type: HOME_INPUT_CHANGED,
    val: value
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case HOME_INPUT_CHANGED:
      return {
        ...state,
        questionScore: (state.question.correct == action.val) ? 1 : 0
      };

    default:
      return state;
  }
}
