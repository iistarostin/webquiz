const initialState = {
  stateIndex: -1,
  question: null,
  score: 0,
  questionScore: 0
};

export default initialState;

