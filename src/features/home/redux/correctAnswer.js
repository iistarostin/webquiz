// Rekit uses a new approach to organizing actions and reducers. That is
// putting related actions and reducers in one file. See more at:
// https://medium.com/@nate_wang/a-new-approach-for-managing-redux-actions-91c26ce8b5da

import {
  HOME_CORRECT_ANSWER,
} from './constants';

export function correctAnswer() {
  return {
    type: HOME_CORRECT_ANSWER,
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case HOME_CORRECT_ANSWER:
      return {
        ...state,
        questionScore: (state.questionScore === 0 ? 1 : 0)
      };

    default:
      return state;
  }
}
