import initialState from './initialState';
import { reducer as nextPageReducer } from './nextPage';
import { reducer as inputChangedReducer } from './inputChanged';
import { reducer as correctAnswerReducer } from './correctAnswer';

const reducers = [
  nextPageReducer,
  inputChangedReducer,
  correctAnswerReducer,
];

export default function reducer(state = initialState, action) {
  let newState;
  switch (action.type) {
    // Handle cross-topic actions here
    default:
      newState = state;
      break;
  }
  /* istanbul ignore next */
  return reducers.reduce((s, r) => r(s, action), newState);
}
