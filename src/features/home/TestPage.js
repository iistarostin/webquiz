import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import * as actions from './redux/actions';

export class TestPage extends Component {
  static propTypes = {
    home: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  };

  renderStart() {
    return (
      <div className="test-page">
        <header className="test-header">
          <h1>Welcome to react quiz app</h1> 
        </header>
        <div className="test-area">
        <button className="btn-next" onClick={this.props.actions.nextPage} >
        Start
        </button>
        </div>
      </div>
    );
  }
  
  renderResults() {
    return (
      <div className="test-page">
        <header className="test-header">
          <h1>Done!</h1> 
        </header>
        <div className="test-area">
        
        <div className="score-area">Your score: {this.props.home.score}</div>
        <button className="btn-next" onClick={this.props.actions.nextPage} >
          To start page
        </button>
        </div>
      </div>
    );
  }

  renderQuestionBody() {
    if (this.props.home.question.questionType === "mc") {
      return (
        <div>
          <div className="question-text"> 
            {this.props.home.question.text}
          </div>
          <div className="mc-answers">
              {this.props.home.question.answers.map(answer => (
              <div>
                <button className="mc-option" onClick={answer.isCorrect ? this.props.actions.correctAnswer : ""}>
                  {answer.text}
                </button>
              </div>
              ))}
          </div>
        </div>
      );
    }
    if (this.props.home.question.questionType === "sa") {
      return (
        <div>
          <div className="question-text"> 
            {this.props.home.question.text}
          </div>
          <div> 
            Answer: <input type="text" onChange={(e) => this.props.actions.inputChanged(e.target.value)}></input>
          </div>
        </div>
      );
    }
  }
  renderQuestion() {
    return (
      <div className="test-page">
        <header className="test-header">
          <h1>{this.props.home.question.title} (Value: {this.props.home.question.value})</h1> 
        </header>
        <div className="test-area">
        {this.renderQuestionBody()}
        <button className="btn-next" onClick={this.props.actions.nextPage} >
        Next question
        </button>
        </div>
      </div>
    );
  }

  render() {
    if (this.props.home.stateIndex === -1) {
      return this.renderStart();
    }
    if (this.props.home.stateIndex === -2) {
      return this.renderResults();
    }
    else {
      return this.renderQuestion();
    }
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    home: state.home,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(TestPage);
